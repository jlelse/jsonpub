package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
)

func allFeedItems(url string) ([]string, error) {
	jsonFeed := &struct {
		Items []struct {
			Url string `json:"url"`
		} `json:"items"`
	}{}
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, errors.New("failed to create req to get json feed")
	}
	req.Header.Add("User-Agent", fmt.Sprintf("%s %s", libName, version))
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, errors.New("failed to get json feed")
	}
	err = json.NewDecoder(resp.Body).Decode(&jsonFeed)
	_ = resp.Body.Close()
	if err != nil {
		return nil, errors.New("failed to parse json feed")
	}
	var allUrls []string
	for _, item := range jsonFeed.Items {
		allUrls = append(allUrls, item.Url)
	}
	return allUrls, nil
}
