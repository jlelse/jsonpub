module git.jlel.se/jlelse/jsonpub

go 1.13

require (
	github.com/andybalholm/cascadia v1.1.0 // indirect
	github.com/dchest/uniuri v0.0.0-20200228104902-7aecb25e1fe5
	github.com/go-fed/httpsig v0.1.1-0.20190924171022-f4c36041199d
	github.com/google/go-cmp v0.4.0 // indirect
	github.com/gorilla/mux v1.7.4
	golang.org/x/crypto v0.0.0-20200302210943-78000ba7a073 // indirect
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a // indirect
	golang.org/x/sys v0.0.0-20200302150141-5c8b2ff67527 // indirect
	willnorris.com/go/webmention v0.0.0-20200126231626-5a55fff6bf71
)
