package main

import (
	"fmt"
	"os"
	"strings"
)

var slash = string(os.PathSeparator)
var baseURL string
var storage = "storage"
var actors = make(map[string]*Actor)
var defaultActor = ""
var telegramBot *Telegram

const libName = "jsonpub"
const version = "0.0.1"

// Setup sets our environment up
func Setup() {
	// Load base url
	baseURL = os.Getenv("BASE_URL")
	if len(baseURL) < 1 {
		fmt.Printf("BASE_URL not configured")
		os.Exit(1)
	}
	// check if it ends with a / and append one if not
	if baseURL[len(baseURL)-1:] != "/" {
		baseURL += "/"
	}
	// print baseURL
	fmt.Println("Base URL:", baseURL)
	cwd, _ := os.Getwd()
	fmt.Println("Storage Location:", cwd+slash+storage)
	// Setup Telegram
	tgChat, tgChatOk := os.LookupEnv("TG_CHAT")
	tgBotToken, tgBotTokenOk := os.LookupEnv("TG_BOT_TOKEN")
	if tgChatOk && tgBotTokenOk {
		telegramBot = &Telegram{
			chat:     tgChat,
			botToken: tgBotToken,
		}
	}
	if telegramBot != nil {
		fmt.Println("Telegram is active")
	}
}

// Get actors from env vars
// NAMES: names of actors
// {{NAME}}_IRI: IRI of actor
// {{NAME}}_PK: Storage location of private Key of actor
func SetupActors() {
	namesString := os.Getenv("NAMES")
	if len(namesString) < 1 {
		fmt.Printf("NAMES not configured")
		os.Exit(1)
	}
	names := strings.Split(namesString, ",")
	for _, name := range names {
		iri := os.Getenv(strings.ToUpper(name) + "_IRI")
		if len(iri) < 1 {
			fmt.Printf(strings.ToUpper(name) + "_IRI not configured")
			os.Exit(1)
		}
		feed := os.Getenv(strings.ToUpper(name) + "_FEED")
		if len(feed) < 1 {
			fmt.Printf(strings.ToUpper(name) + "_FEED not configured")
			os.Exit(1)
		}
		pk := os.Getenv(strings.ToUpper(name) + "_PK")
		if len(pk) < 1 {
			fmt.Printf(strings.ToUpper(name) + "_PK not configured")
			os.Exit(1)
		}
		actor, err := GetActor(name, iri, feed, pk)
		actors[name] = &actor
		if err != nil {
			fmt.Printf(err.Error())
			os.Exit(1)
		}
	}
	defaultName := os.Getenv("DEFAULT_NAME")
	if len(defaultName) < 1 {
		fmt.Printf("DEFAULT_NAME not configured")
	} else {
		defaultActor = defaultName
	}
}
