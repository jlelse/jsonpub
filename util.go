package main

import (
	"net/http"
)

const ContentTypeAs2 = "application/activity+json"

func isSuccess(code int) bool {
	return code == http.StatusOK ||
		code == http.StatusCreated ||
		code == http.StatusAccepted ||
		code == http.StatusNoContent
}

func context() [1]string {
	return [1]string{"https://www.w3.org/ns/activitystreams"}
}
